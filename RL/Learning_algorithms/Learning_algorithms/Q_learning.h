#pragma once
#include <vector>
#include <utility>
#include <algorithm>
#include <iostream>
#include <iomanip>
#include <random>
#include <iterator>



#ifndef WALL_TYPE
    #define WALL_TYPE '#'
#endif // !WALL_TYPE

#ifndef GOAL_TYPE
    #define GOAL_TYPE '*'
#endif // !WALL_TYPE


enum class Action
{
    RIGHT = 0,
    LEFT = 1,
    DOWN = 2,
    UP = 3,
    NO_ACTION = 4
};

struct State
{
public:
    std::vector<Action> actions;
    char state_type;
    std::vector<double> Q_values;   
    double state_reward;
    struct pos
    {
        int x;
        int y;
    }state;

    bool operator == (const State& other) { return (state.x == other.state.x && state.y == other.state.y); }
};



class Q_learning
{
public:
    Q_learning(int size_x, int size_y, double epsilon, double learning_rate, double discount_rate, double theta, double transition_reward);
    ~Q_learning();
    
    void set_state(State other);
    void print_state_space();
    void print_environment();
    void run_episode(int episodes, int number_of_goals, int64_t max_trials);
    void make_default_state(State& state);
    void add_goal_to_map(State goal);


private:
    void init();
    double get_reward(const State& state);
    double get_optimal_value(const State & state);
    Action get_policy_action(State& state);
    State& get_next_state(State S, Action a);
    void update_Q_value(double Q_max, Action policy_action, State& state, State& next_state);
    void reset_all_goals();
   
    int state_space_size_x;
    int state_space_size_y;
    std::vector<std::vector<State>> map;
    double epsilon;
    double learning_rate;
    double discount_ratio;
    double theta;    
    double transition_reward;
    std::mt19937_64 generator;
    std::vector<State> goals;

};

