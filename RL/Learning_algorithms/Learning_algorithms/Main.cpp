#include "Q_learning.h"



int main()
{
    Q_learning DUT(13, 13, 0.2, 0.3, 0.9, 0.001, -0.4);
    DUT.add_goal_to_map({ { Action::NO_ACTION, Action::NO_ACTION }, '*', {0.9}, 10, {8,8} });
    DUT.add_goal_to_map({ { Action::NO_ACTION, Action::NO_ACTION }, '*',{ 0.9 }, 10,{ 5,5 } });
    //DUT.set_state({ { Action::RIGHT, Action::DOWN, Action::LEFT, Action::UP }, '^',{ 0.9 }, 10,{ 3,3 } });
    for (int i = 0; i < 13; i++)
    {
        for (int j = 0; j < 13; j++)
        {
            if (j == 0 || i == 0 || j == 12 || i == 12)
            {
                DUT.set_state({ { Action::NO_ACTION, Action::NO_ACTION }, '#', {-1 }, -0.1, { i,j } });
            }
        }
    }
    DUT.print_state_space();
    DUT.print_environment();

   DUT.run_episode(1000, 2, 500);
   DUT.print_state_space();
   DUT.print_environment();

}