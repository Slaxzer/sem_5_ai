#include "Q_learning.h"



Q_learning::Q_learning(int size_x, int size_y, double epsilon, double learning_rate, double discount_rate, double theta, double transition_reward)
    : state_space_size_x{size_x}
    , state_space_size_y{size_y}
    , map()
    , epsilon{epsilon}
    , learning_rate{learning_rate}
    , discount_ratio{discount_rate}
    , theta{theta}
    , transition_reward{transition_reward}
    , generator()    
    , goals()
{
    init();
}


Q_learning::~Q_learning()
{
}


void Q_learning::init()
{
    map.reserve(state_space_size_x);

    for(int x = 0; x < state_space_size_x; x++)
    {
        map.push_back(std::vector<State>());
        for (int y = 0; y < state_space_size_y; y++)
        {
            map[x].push_back({ { Action::LEFT, Action::RIGHT, Action::DOWN, Action::UP }, '-',{ 0.0, 0.0, 0.0, 0.0 }, transition_reward, {x,y} });
        }
    }
}


void Q_learning::set_state(State new_state)
{
    map[new_state.state.x][new_state.state.y] = new_state;
}

void Q_learning::print_state_space()
{
    static int state_width = 7;
    std::cout << "Expected rewards is: " << std::endl;
    //std::cout << std::setw(map.size())
    std::cout << std::setfill('#') << std::setw((state_space_size_x + 1) * state_width + 1) << '#' << std::endl << std::setfill(' ');
    for (const auto& row : map)
    {
        std::cout << '#';
        for (const auto& column : row)
        {                      
            std::cout << std::setw(state_width) << std::fixed << std::setprecision(2) <<*std::max_element(column.Q_values.begin(), column.Q_values.end());
        }
        std::cout << std::setw(state_width) << '#' << std::endl;
    }
    std::cout << std::setfill('#') << std::setw((state_space_size_x + 1) * state_width + 1) << '#' << std::endl << std::setfill(' ');
}

void Q_learning::print_environment()
{
    std::cout << std::setfill('#') << std::setw((state_space_size_x + 1) * 5 + 1) << '#' << std::endl << std::setfill(' ');
    for (const auto& row : map)
    {
        std::cout << '#';
        for (const auto& column : row)
        {
            std::cout << std::setw(5) << column.state_type;
        }
        std::cout << std::setw(5) << '#' << std::endl;
    }
    std::cout << std::setfill('#') << std::setw((state_space_size_x + 1) * 5 + 1) << '#' << std::endl << std::setfill(' ');
}

void Q_learning::run_episode(int episodes, int number_of_goals, int64_t max_trials)
{
   
    // TODO RESET ALL GOALS WHEN AN EPISODE HAS ELAPSED.

    for (int i = 0; i < episodes; i++)
    {
        State* state = &map[2][2];
        int goals_remaining = number_of_goals;
        int64_t trials_in_episode = 0;
        reset_all_goals();

        while(goals_remaining > 0 && trials_in_episode < max_trials)
        {
            Action A_policy = get_policy_action(*state);
            State& new_state = get_next_state(*state, A_policy);
            double Q_max = get_optimal_value(new_state);
            update_Q_value(Q_max, A_policy, *state, new_state);
            state = &new_state;
            if (state->state_type == GOAL_TYPE)
            {
                goals_remaining--;
                make_default_state(*state);
            }
            trials_in_episode++;
        }
    }
    reset_all_goals();
}

void Q_learning::make_default_state(State& state)
{
    state = { { Action::LEFT, Action::RIGHT, Action::DOWN, Action::UP }, '-', { 0.0, 0.0, 0.0, 0.0 }, transition_reward, { state.state.x, state.state.y} };
}

void Q_learning::add_goal_to_map(State goal)
{
    goals.push_back(goal);
}



double Q_learning::get_reward(const State& state)
{
    return state.state_reward;
}

double Q_learning::get_optimal_value(const State & state)
{  
    
    return *std::max_element(state.Q_values.cbegin(), state.Q_values.cend());
}

Action Q_learning::get_policy_action(State& state)
{    
    static std::uniform_real_distribution<double> uni_dist(0.0, 1.0);
    std::uniform_int_distribution<int64_t> random_action_dist(0, 3);
    int64_t action_idx;

    if (uni_dist(generator) > epsilon)
    {
        //Exploit ie. choose best action
        action_idx = std::distance(state.Q_values.begin(), std::max_element(state.Q_values.begin(), state.Q_values.end()));
    }
    else
    {
        //Explore ie. take random action
        action_idx = random_action_dist(generator);
        //std::cout << "This action was chosen: " << action_idx << std::endl;
    }
    return (Action)action_idx;   
}

State& Q_learning::get_next_state(State S, Action a)
{
    //std::cout << (int)a << std::endl << "Current state: " << S.state.x << " , " << S.state.y << std::endl;
    State* next_state = &map[S.state.x][S.state.y];
    if (S.state.y < state_space_size_y - 1 && S.state.y > 0 && S.state.x < state_space_size_x - 1 && S.state.x > 0)
    {
        switch (a)
        {
        case Action::RIGHT:
            next_state = &map[S.state.x + 1][S.state.y];
            break;

        case Action::DOWN:
            next_state = &map[S.state.x][S.state.y - 1];
            break;

        case Action::LEFT:
            next_state = &map[S.state.x - 1][S.state.y];
            break;

        case Action::UP:
            next_state = &map[S.state.x][S.state.y + 1];
            break;
        }
        if(next_state->state_type == WALL_TYPE) return map[S.state.x][S.state.y];
    }
    //std::cout << (int)a << std::endl << "Current state: " << next_state->state.x << " , " << next_state->state.y << std::endl;
    return *next_state;    
   
}

void Q_learning::update_Q_value(double Q_max, Action policy_action, State& state, State & next_state)
{
    double reward = get_reward(next_state);
    state.Q_values[(int)policy_action] += learning_rate * (reward + discount_ratio * Q_max - state.Q_values[(int) policy_action]);    
}

void Q_learning::reset_all_goals()
{
    for (const auto goal : goals)
    {
        map[goal.state.x][goal.state.y] = goal;
    }
}




/*


State* next_state;

next_state = &get_next_state(state, (Action)action_idx);

double reward = get_reward(*next_state);

if (state.state.x == next_state->state.x && state.state.y == next_state->state.y)
{
// We hit a wall, danm!
state.Q_values[action_idx] += reward;
}
else
{
// We did not hit a wall! We need to take the next optimal move!!!
double Q_optimal = get_optimal_action(*next_state);
state.Q_values[action_idx] += learning_rate * (reward + discount_ratio * Q_optimal - state.Q_values[action_idx]);
}

//std::cout << action_idx << std::endl << "Current state: " << next_state->state.y << " , " << next_state->state.x << std::endl;
return *next_state;

*/